package com.example.maps_app_alandelacruz

data class Marcador(
    val latitude: Double,
    val longitude: Double,
    val namemarker: String,
    val imgname: String
)

