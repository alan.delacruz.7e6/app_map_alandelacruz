package com.example.maps_app_alandelacruz

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.maps_app_alandelacruz.databinding.FragmentLocationsListBinding

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore


class LocationsListFragment : Fragment() {
    private val db = FirebaseFirestore.getInstance()
    private lateinit var adapter: LocationAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var marcadorsList : MutableList<Marcador>
    var auth = FirebaseAuth.getInstance()
    lateinit var binding: FragmentLocationsListBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLocationsListBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_locations_list, container, false)
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataInitialize()
        val layoutManager = LinearLayoutManager(context)
        recyclerView = view.findViewById(R.id.recyclerview)
        recyclerView.layoutManager = layoutManager
        recyclerView.setHasFixedSize(true)
        adapter = LocationAdapter(marcadorsList)
        recyclerView.adapter = adapter
        val swipeToDeleteCallback = object : SwipeToDeleteCallback(){
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    // Eliminar el marcador de la base de datos
                    val documentId = marcadorsList[position].namemarker
                    db.collection("users").document(auth.currentUser?.email!!)
                        .collection("marcadores").document(documentId)
                        .delete()
                        .addOnSuccessListener {
                            // Eliminar el marcador de la lista y actualizar el RecyclerView
                            marcadorsList.removeAt(position)
                            recyclerView.adapter?.notifyItemRemoved(position)
                        }
                        .addOnFailureListener { e ->
                            Log.w(TAG, "Error deleting document", e)
                        }
                }
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeToDeleteCallback)
        itemTouchHelper.attachToRecyclerView(recyclerView)

    }




    private fun dataInitialize(){
        marcadorsList = mutableListOf()
        db.collection("users").document(auth.currentUser?.email!!).collection("marcadores").get()
            .addOnSuccessListener { documents ->
                synchronized(marcadorsList) {
                    var ubication: Marcador
                    for (document in documents){
                        ubication = Marcador(document.get("Latitude").toString().toDouble(), document.get("Longitude").toString().toDouble(), document.get("namemarker").toString(), document.get("imgname").toString())
                        marcadorsList.add(Marcador(ubication.latitude, ubication.longitude, ubication.namemarker, ubication.imgname))
                    }
                    adapter.notifyDataSetChanged()
                }
            }
    }
}