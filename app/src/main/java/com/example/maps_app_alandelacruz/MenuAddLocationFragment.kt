package com.example.maps_app_alandelacruz


import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.navigation.fragment.findNavController
import com.example.maps_app_alandelacruz.databinding.FragmentMenuAddLocationBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.text.SimpleDateFormat
import java.util.*

private val db = FirebaseFirestore.getInstance()

class MenuAddLocationFragment : Fragment() {
    lateinit var imageUri: Uri
    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            result ->
        if (result.resultCode == RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                imageUri = data.data!!
                binding.imgbutton.setImageURI(imageUri)
            }
        }
    }

    lateinit var binding: FragmentMenuAddLocationBinding
    var auth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMenuAddLocationBinding.inflate(layoutInflater)
        binding.btnCamera.setOnClickListener {
            findNavController().navigate(R.id.action_menuAddLocationFragment_to_addMarkerScreen)
        }
        MapscreenFragment.coordinates?.let {
            binding.numlatitude.setText(it.latitude.toString())
            binding.numlongitude.setText(it.longitude.toString())
        }

        binding.savebutton.setOnClickListener {
            Toast.makeText(requireContext(), "S'ha desat correctament", Toast.LENGTH_SHORT).show()
            val formatter = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
            val now = Date()
            val fileName = formatter.format(now)
            val storage = FirebaseStorage.getInstance().getReference("images/$fileName")
            storage.putFile(imageUri)
                .addOnSuccessListener {
                    binding.imgbutton.setImageURI(null)
                    Toast.makeText(requireContext(), "Image uploaded!", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener { e -> println("---------------$e")
                    Toast.makeText(requireContext(), "Image not uploaded!", Toast.LENGTH_SHORT).show()
                }
            db.collection("users").document(auth.currentUser?.email!!).collection("marcadores").document(binding.markertitle.text.toString()).set(
                hashMapOf("Latitude" to binding.numlatitude.text.toString(), "Longitude" to binding.numlongitude.text.toString(), "namemarker" to binding.markertitle.text.toString(), "imgname" to fileName))

        }
        binding.selectphotobutton.setOnClickListener {
            selectImage()
        }


        return binding.root
    }

    private fun selectImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        resultLauncher.launch(intent)
    }

}

