package com.example.maps_app_alandelacruz

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.maps_app_alandelacruz.databinding.FragmentRegisterBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore


private val db = FirebaseFirestore.getInstance()

class RegisterFragment : Fragment() {
    lateinit var binding: FragmentRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegisterBinding.inflate(layoutInflater)
        val email = binding.regEmail.text
        val password = binding.regPassword.text

        binding.registerButton.setOnClickListener {
            if (binding.regPassword.text.toString() == binding.confirmpswd.text.toString()){
                FirebaseAuth.getInstance().
                createUserWithEmailAndPassword(email.toString(), password.toString())
                    .addOnCompleteListener {
                        if(it.isSuccessful){
                            val emailLogged = it.result?.user?.email
                            db.collection("users").document(emailLogged!!).set(
                                hashMapOf("email" to binding.regEmail.text.toString())
                            )
                            goToHome()

                        }
                        else{
                            Toast.makeText(requireContext(), "Error al registrar l'usuari", Toast.LENGTH_SHORT).show()
                        }
                    }
            }
            else{
                binding.messageError.text = "Las contraseñas no son iguales!"
            }
        }
        binding.btnIniciarsesion.setOnClickListener {
            findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
        }
        // Inflate the layout for this fragment
        return binding.root
    }

    fun goToHome(){
        findNavController().navigate(R.id.action_registerFragment_to_mapscreenFragment)
    }


}