package com.example.maps_app_alandelacruz

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.maps_app_alandelacruz.databinding.ListItemBinding
import com.google.firebase.storage.FirebaseStorage
import java.io.File

class LocationAdapter(private val markersList : MutableList<Marcador>) : RecyclerView.Adapter<LocationAdapter.MyViewHolder>() {

    private lateinit var mListener : onItemClickListener

    interface onItemClickListener{
        fun onItemClick(position: Int)
    }

    fun setOnItemClickListener(listener: onItemClickListener){
        mListener = listener
    }

    fun deleteItem(i: Int){
        markersList.removeAt(i)
        notifyDataSetChanged()
    }

    inner class MyViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val binding = ListItemBinding.bind(itemView)
        val latitude : TextView = itemView.findViewById(R.id.textlatitud)
        val longitude : TextView = itemView.findViewById(R.id.textlongitud)
        val markerimg : ImageView = itemView.findViewById(R.id.title_image)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = markersList[position]
        with(holder){
            binding.textlatitud.text = currentItem.latitude.toString()
            binding.textlongitud.text = currentItem.longitude.toString()
            binding.titlemarker.text = currentItem.namemarker

            val storage = FirebaseStorage.getInstance().reference.child("images/${currentItem.imgname}")
            val localFile = File.createTempFile("temp", "jpeg")
            storage.getFile(localFile).addOnSuccessListener {
                val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                binding.titleImage.setImageBitmap(bitmap)

            }.addOnFailureListener{
                println("ERROR IMAGE")
//                Toast.makeText(requireContext(), "Error downloading image!", Toast.LENGTH_SHORT).show()
            }
            //binding.titleImage.setImageResource(currentItem.markerimg)
        }
    }

    override fun getItemCount(): Int {
        return markersList.size
    }




}